###Installing the docker-registry into the CentOS 

This role was tested on CentOS 7

####Requirements
1. Docker-engine should be installed before role applying

####Variables, which user should declare in any case

* TLS_CERTIFICATE - registry certificate
* TLS_KEY - registry private key
* rsync_destination - the destination, where rsync should sync the registry folder

For example:
```
  vars:
    TLS_CERTIFICATE: "files/certs/server.crt"
    TLS_KEY: "files/certs/server.key" 
    rsync_destination: "/backup"
```

####The cron task description

By default the task will be scheduled in "0 1 * * *"

To redefine the cron schedule time the user should redefine the variable: 
```
cron_time:
    minute: "0"
    hour: "1"
    day: "*"
    month: "*"
    weekday: "*"
```

####The rsync command description
Rsync stores one copy of registry

An example:
```
$ rsync -azvq --delete /var/lib/registry /backup
```